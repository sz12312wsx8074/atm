package com.example.atm

import android.app.Activity
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_nickname.*

class NicknameActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nickname)

        tm_nick_btn.setOnClickListener {
            setInfo()
        }
    }

    fun setInfo() {
        val nick = tm_nick.text.toString()
        if (nick != "") {
            getSharedPreferences("atm", Context.MODE_PRIVATE)
                .edit()
                .putString("nick", nick)
                .apply()
            setResult(Activity.RESULT_OK)
            finish()
        } else {
            AlertDialog.Builder(this)
                .setTitle("警告")
                .setMessage("請輸入資訊")
                .setPositiveButton("確定", null)
                .show()
        }
    }
}
