package com.example.atm

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.annotation.RequiresApi
import com.example.atm.R.layout.activity_main

class MainActivity : AppCompatActivity() {
    var login = false
    var infoComplete = false
    val REQUEST_LOGIN = 66
    val REQUEST_MAP = mapOf(
        "nick" to 1,
        "gender" to 2,
        "age" to 3
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(activity_main)

        if (!login) {
            val intent = Intent(this, LoginActivity::class.java)
            startActivityForResult(intent, REQUEST_LOGIN)
        } else if (!infoComplete) {
            checkInfo()
        }
    }

//    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
//        menuInflater.inflate(R.menu.menu_main, menu)
//        return super.onCreateOptionsMenu(menu)
//    }
//
//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        if (item.itemId == R.id.action_content) {
//
//        }
//        return super.onOptionsItemSelected(item)
//    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_LOGIN) {
                login = true
            }

            if (!infoComplete) {
                checkInfo()
            }
        } else {
            finish()
        }
    }

    private fun checkInfo() {
        Log.d("TEST", "check")
        val data = getSharedPreferences("atm", Context.MODE_PRIVATE)
        var finishInfoCount = 0
        val list = mapOf(
            "nick" to NicknameActivity::class.java,
            "gender" to GenderActivity::class.java,
            "age" to AgeActivity::class.java
        )

        list.forEach {
            (infoKey, infoActivity) ->
                if(data.getString(infoKey, "") == "") {
                    REQUEST_MAP[infoKey]?.run {
                        startActivityForResult(
                            Intent(this@MainActivity, infoActivity),
                            this
                        )
                        return
                    }
                }
                finishInfoCount++
        }

        if (finishInfoCount == list.count()) {
            infoComplete = true
        }
    }
}
