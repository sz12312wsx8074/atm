package com.example.atm

import android.app.Activity
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_age.*

class AgeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_age)

        tm_age_btn.setOnClickListener {
            setInfo()
        }
    }

    fun setInfo() {
        val age = tm_age.text.toString()
        if (age != "") {
            getSharedPreferences("atm", Context.MODE_PRIVATE)
                .edit()
                .putString("age", age)
                .apply()
            setResult(Activity.RESULT_OK)
            finish()
        } else {
            AlertDialog.Builder(this)
                .setTitle("警告")
                .setMessage("請輸入資訊")
                .setPositiveButton("確定", null)
                .show()
        }
    }
}
