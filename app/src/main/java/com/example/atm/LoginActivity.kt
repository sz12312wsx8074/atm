package com.example.atm

import android.app.Activity
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.example.atm.R.layout.activity_login
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(activity_login)

        getSharedPreferences("atm", Context.MODE_PRIVATE)
            .getString("username", "")?.run {
                tm_username.setText(this)
            }
    }

    fun login(v : View) {
        val username = tm_username.text.toString()
        val password = tm_password.text.toString()

        if (username == "te" && password == "qwe123") {
            getSharedPreferences("atm", Context.MODE_PRIVATE)
                .edit()
                .putString("username", username)
                .apply()

            Toast.makeText(this, "成功！", Toast.LENGTH_LONG).show()
            intent.putExtra("USETNAME", username)
            intent.putExtra("PASSWORD", password)
            setResult(Activity.RESULT_OK)
            finish()
        } else {
            Toast.makeText(this, "錯誤啦", Toast.LENGTH_LONG).show()
        }
    }

    fun cancel (v: View) {
        setResult(Activity.RESULT_CANCELED)
        finish()
    }
}
