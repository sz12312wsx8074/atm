package com.example.atm

import android.app.Activity
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_gender.*

class GenderActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gender)

        tm_gender_btn.setOnClickListener {
            setInfo()
        }
    }

    fun setInfo() {
        val gender = tm_gender.text.toString()
        val genderList = arrayOf("男", "女")

        if ((gender != "") && (genderList.contains(gender))) {
            getSharedPreferences("atm", Context.MODE_PRIVATE)
                .edit()
                .putString("gender", gender)
                .apply()
            setResult(Activity.RESULT_OK)
            finish()
        } else {
            AlertDialog.Builder(this)
                .setTitle("警告")
                .setMessage("請輸入正確資訊")
                .setPositiveButton("確定", null)
                .show()
        }
    }
}
